# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 12:28:40 2017

@author: Migue 
"""

def read_fasta_file_as_list_of_pairs(fichero):
    identificador=''
    secuencia=''
    
    palabras=[]
    
    for line in open(fichero):
        if line[0]=='>':
            if identificador!='':
                palabras.append([identificador,secuencia])
            line=line.split("\n")
            identificador=line[0].rstrip()
            secuencia=''
        else:
            secuencia=secuencia+line.rstrip()
    palabras.append([identificador,secuencia])
            
    return palabras


def read_fasta_file_as_dictionary(fichero):
    identificador = ''
    secuencia = ''

    identificadores = []
    secuencias = []

    with open(fichero) as inputfile:
        for line in inputfile:
            if line[0] == '>':
                if identificador != '':
                    identificadores.append(identificador)
                    secuencias.append(secuencia)
                line = line.split("\n")
                identificador = line[0].rstrip()
                secuencia = ''

            else:
                secuencia = secuencia + line.rstrip()

        dictionary = dict(zip(identificadores, secuencias))
        print(dictionary)